package com.improving.codechallenge.repository;

import com.improving.codechallenge.model.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    List<ApplicationUser> findByOrderByLastNameAsc();
    List<ApplicationUser> findByFirstNameIgnoreCaseAndLastNameIgnoreCase(String firstName, String lastName);
}
