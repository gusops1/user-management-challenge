package com.improving.codechallenge.service;

import com.improving.codechallenge.model.ApplicationUser;
import com.improving.codechallenge.repository.ApplicationUserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl  {
    private ApplicationUserRepository applicationUserRepository;

    public List<ApplicationUser> getUserList(){
        return applicationUserRepository.findByOrderByLastNameAsc();
    }
}
