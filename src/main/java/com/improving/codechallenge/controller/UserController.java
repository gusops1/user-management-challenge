package com.improving.codechallenge.controller;

import com.improving.codechallenge.model.ApplicationUser;
import com.improving.codechallenge.repository.ApplicationUserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {
    private ApplicationUserRepository applicationUserRepository;

    public UserController(ApplicationUserRepository applicationUserRepository) {
        this.applicationUserRepository = applicationUserRepository;
    }

    @GetMapping("/")
    public List<ApplicationUser> getUserList(){
        return applicationUserRepository.findByOrderByLastNameAsc();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApplicationUser> getUser(@PathVariable Long id){
        Optional<ApplicationUser> appUser = applicationUserRepository.findById(id);

        if(appUser.isPresent()){
            return new ResponseEntity<ApplicationUser>(appUser.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<ApplicationUser>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/")
    public ResponseEntity<ApplicationUser> signUp(@RequestBody ApplicationUser user) throws Exception {
        List<ApplicationUser> existingUsers = applicationUserRepository.findByFirstNameIgnoreCaseAndLastNameIgnoreCase(user.getFirstName(), user.getLastName());

        if(existingUsers.size() == 0) {
            applicationUserRepository.save(user);

            return new ResponseEntity<ApplicationUser>(user, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<ApplicationUser>(HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApplicationUser> deleteUser(@PathVariable Long id){
        Optional<ApplicationUser> user = applicationUserRepository.findById(id);

        if (user.isPresent()) {
            applicationUserRepository.delete(user.get());

            return new ResponseEntity<ApplicationUser>(HttpStatus.OK);
        } else {
            return new ResponseEntity<ApplicationUser>(HttpStatus.NOT_FOUND);
        }
    }
}
