package com.improving.codechallenge.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "app_user")
public class ApplicationUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "first")
    @Size(min = 1)
    private String firstName;
    @Column(name = "name")
    @Size(min = 1)
    private String lastName;
}
